const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController.js");
const auth = require("../auth");

// Route for creating a course
router.post("/", auth.verify, (req, res) => {

    const data = {
        course: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    console.log(data)
    if (data.isAdmin) {

        courseController.addCourse(data).then(resultFromController => res.send(resultFromController))

    // user is not Admin
    } else {
        res.send(false)
    }
    
});

// Retrieveing all the courses
router.get("/allCourses", (req, res) => {
    courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});


//Retrieving all Active Courses
router.get("/", (req, res) => {
    courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Retrieveing a specific courses
router.get("/:courseId", (req, res) => {
    console.log(req.params.courseId)
    courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});

// Updating a course
// JWT verificaiton is needed to ensure that a user is logged in before updating a course
router.put("/:courseId", auth.verify, (req, res) => {

    // To know if the logged in user is an admin or not
    const data = auth.decode(req.headers.authorization);

    if (data.isAdmin) {
            courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
        } else {
            res.send(false)
        }

});

// Route to archive a course

router.patch("/:courseId/archive", auth.verify, (req, res) => {
    courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

module.exports = router;