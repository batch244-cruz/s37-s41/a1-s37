const Course = require("../models/Course");

// Create a new course
/*
    Steps:
    1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
    2. Save the new User to the database
*/

module.exports.addCourse = (data) => {

    if(data.isAdmin) {

        // Uses the information from the request body to provide all the necessary information
        let newCourse = new Course ({
            name: data.course.name,
            description: data.course.description,
            price: data.course.price
        });

        return newCourse.save().then((course, error) => {
            // If course creation failed
            if (error){
                return false
            // If course creation is successful
            } else{
                return true;
            }
        })
        // User is not an admin
    } else {
        return false
    }
};

// Retrieve all courses

module.exports.getAllCourses = () => {
    return Course.find({}).then(result => {
        return result;
    })
}

// Retrieve all active coruses
module.exports.getAllActive = () => {
    return Course.find({isActive:true}).then(result => {
        return result;
    })
}

// Retrieving a specific course
module.exports.getCourse = (reqParams) => {
    return Course.findById(reqParams.courseId).then(result => {
        return result
    })
}

// Updating a course
module.exports.updateCourse = (reqParams, reqBody) => {
    
       let updatedCourse = {
            name: reqBody.name,
            description: reqBody.description,
            price: reqBody.price
        };

        // Syntax findByIdAndUpdate(documents ID, Updates to be applied)
        return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {

            if (error) {
                return false;

            }else {
                return true;
            }
        })
}

// Archiving Course
module.exports.archiveCourse = (reqParams, reqBody) => {

    let updateActiveField = {
        isActive : reqBody.isActive
    };

    return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {

        if (error) {
            return false;
        } else {
            return true;
        }
    })
};